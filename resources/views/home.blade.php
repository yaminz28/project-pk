@extends('layouts.app')

@section('content')
 <!-- Small boxes (Stat box) -->
 <div class="row">
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
            <div class="inner">
                <h3> {{ $design->count() }}</h3>

                <p>Total Your Design</p>
            </div>
            <div class="icon">
            <i class="icon ion-md-contacts"></i>
            </div>
            <a href="/designers/" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
</div>
<!-- /.row -->
<!-- Main row -->



@endsection
