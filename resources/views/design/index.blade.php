@extends('layouts.app')

@section('content')
<section class="content">
    <div class="row">
      <div class="col-12">


        <div class="card">
          <div class="card-header">
            <h3 class="card-title">List Your Design</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Pekerjaan</th>
                <th>Umur</th>
                <th>Warna</th>
                <th>Gaya Design</th>
                <th>Jumlah Orang</th>
                <th>Image</th>
                <th colspan="2">Action</th>
              </tr>
              </thead>
              <tbody>
                  @foreach ($design as $d)
              <tr class="text-center">
                <td>{{$d->id}}</td>
                <td>{{$d->nama}}</td>
                <td>{{$d->pekerjaan}}</td>
                <td>{{$d->umur}}</td>
                <td>{{$d->warna}}</td>
                <td>{{$d->category->name}}</td>
                <td>{{$d->jumlah_orang}}</td>
                <td>{{$d->design_image->count()}}</td>
                <td>
                    <a class="btn btn-info" href="{{route('designers.edit', $d->id)}}">Edit</a>
                  </td>
                  <td>
                    <form action="{{ route('designers.destroy', $d->id) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-info">Hapus</button>
                    </form>
                  </td>
              </tr>
              @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@endsection
