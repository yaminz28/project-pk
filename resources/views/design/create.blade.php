@extends('layouts.app')

@section('content')
   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Form Create Design</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form enctype="multipart/form-data" method = "post" action = "{{route('designers.store')}}" id = "designform" role = "form">
                    {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="InputCreator">Creator</label>
                  <input type="text" class="form-control" id="creator" name="creator" value="{{Auth::user()->id}}"readonly>
                  </div>
                   <div class="form-group">
                    <label for="Input Category">Category</label>
                    <select class="form-control" id="kategoriSort" name="kategoriSort" required>
                        <option value="" disabled selected> Pilih Kategori</option>
                        @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                  </div>
                  {{-- Nama --}}
                  <div class="form-group">
                    <label for="InputNamaDesign">Nama</label>
                    <input type="text" class="form-control" name="nama" placeholder="Nama Design">
                  </div>
                  {{-- End Nama --}}
                  {{-- Pekerjaan --}}
                        <div class="form-group">
                            <label for="InputPekerjaan">Pekerjaan</label>
                            <input type="text" class="form-control" name="pekerjaan" placeholder="Tipe Pekerjaan Yang Cocok Untuk Design Ini">
                          </div>
                          {{-- END PEKERJAAN --}}
                          {{-- Umur --}}
                      <div class="form-group">
                        <label for="InputUmur">Umur</label>
                    <input type="text" class="form-control" name="umur" placeholder="Umur Yang Cocok Untuk Design Ini">
                  </div>
                  {{-- End Umur --}}
                  {{-- Warna --}}
                  <div class="form-group">
                    <label for="InputUmur">Warna</label>
                <input type="text" class="form-control" name="warna" placeholder="Umur Yang Cocok Untuk Design Ini">
              </div>
              {{-- End Warna --}}
              {{-- Gaya Design --}}
                  <div class="form-group">
                    <label for="InputGayaDesign">Gaya Design</label>
                    <select class="form-control" id="designSort" name="designSort" required>
                        <option value="" disabled selected> Pilih Gaya Design</option>
                        @foreach ($categories as $category)
                        <option value="{{$category->name}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                  </div>
                  {{-- End Gaya Design --}}
                  {{-- Jumlah Orang --}}
                  <div class="form-group">
                    <label for="InputJumlahOrang">Jumlah Orang</label>
                    <input type="text" class="form-control" name="jumlah" placeholder="Jumlah Orang Yang Cocok Untuk Rumah ini">
                  </div>
                  {{-- End Jumlah orang --}}
                  <div class="form-group">
                    <label for="InputFile">Tambah Gambar</label>
                    <input class="uploadpicker" data-image="#files-add" accept="image/*" name="file" type="file" multiple>
                    <div id="files-add"></div>
                </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-submit float-right" style="background-color:blue; color:white">Simpan</button>
                </div>
              </form>
              </div>
              <!-- /.card -->


          </div>
          <!-- /.col -->

          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

    @endsection
    @section('script')
    <script>
        $(function(){
            previewFile('file')
            var input = $("input.uploadpicker");
              input.fileinput({
                  theme: "fas",
                  uploadUrl: "{{ url('/upload') }}",
                  uploadExtraData: function() {
                      return {
                          _token: "{{ csrf_token() }}",
                          type: 'design'
                      };
                  },
                  initialPreviewAsData: true, // identify if you are sending preview data only and not the markup
                  overwriteInitial: false,
                  allowedFileExtensions: ['jpg', 'png', 'gif', 'jpeg'],
                  maxFileSize:2048,
                  maxTotalFileCount: 10
              });

              input.on('fileuploaded', function(event, response, id, fileId) {
                  $($(this).data('image')).append("<input id='"+response.response.initialPreviewConfig[0].key+"' type='hidden' name='images[]' value='"+response.response.initialPreview[0]+"'></input");
              });

              input.on('filedeleted', function(event, key, data) {
                  $($(this).data('image')+" input#"+key).remove();
              });

              $('button.btn-submit').on('click', function(e){
                e.preventDefault()

                var result = { };
                $.each($('form#designform').serializeArray(), function() {
                    result[this.name] = this.value;
                });
                if(typeof(result.kategoriSort) == "undefined" || result.kategoriSort === null || result.kategoriSort == ''){
                    alert('Anda belum memilih Kategori Artikel')
                }else if (result.nama == '') {
                    alert('Anda belum mengisi Nama Design')
                }else if(result.pekerjaan == ''){
                    alert('Anda belum mengisi Pekerjaan')
                }else if(result.umur == ''){
                    alert('Anda belum mengisi Umur')
                }else if(result.warna == ''){
                    alert('Anda belum mengisi Warna')
                }else if(result.jumlah == ''){
                    alert('Anda belum mengisi Jumlah Orang')
                }else if(typeof(result.designSort) == "undefined" || result.designSort === null || result.designSort == ''){
                    alert('Anda belum memilih Design Artikel')
                }
                else{
                    var image = $('form#designform .uploadpicker').data('image');

                    let countImage = $(image+" input").length
                    let previewImage = $('form#designform .file-preview-frame').length
                    let existingImage = (typeof $('form#designform input[name="count_existing_image"]').val() !== 'undefined' ? $('form#designform input[name="count_existing_image"]').val() : 0)
                    if ((previewImage / 2) < 1) {
                        alert('Anda belum memilih gambar')
                    }else{
                        if ((previewImage / 2) > existingImage) {
                            if (countImage < 1) {
                                alert('Gambar belum diupload, klik button upload terlebih dahulu sebelum menyimpan');
                            }else if((previewImage / 2) == countImage){
                                $('form#designform').submit();
                            }else{
                                alert('Gambar belum diupload, klik button upload terlebih dahulu sebelum menyimpan');
                            }
                        }else if((previewImage / 2) == existingImage){
                            $('form#designform').submit();
                        }
                    }
                }
            })
        })
        </script>
    @endsection
