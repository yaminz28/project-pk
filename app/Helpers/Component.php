<?php

if (!function_exists('name_comp')) {
    function name_comp($type){
        if($type == 'single-image'){
            return 'Single Image';
        }else if($type == 'multiple-image'){
            return 'Multiple Image';
        }else if($type == 'image-slider'){
            return 'Image Slider';
        }else if($type == 'text-block'){
            return 'Text Block';
        }else if($type == 'text-editor'){
            return 'Summernote Text Editor';
        }else if($type == 'button'){
            return 'Button';
        }else if($type == 'ticket-box'){
            return 'Ticket Box';
        }else if($type == 'newsletter-box'){
            return 'Newsletter Box';
        }
    }
}

if (!function_exists('component_edit')) {
    function component_edit($type, $data, $key){
        if($type == 'single-image'){
            $html = '';
            $html .= '<input class="uploadpicker-single-edit" data-image="#files-'.$key.'" accept="image/*" name="file" type="file" multiple>
                      <div id="files-'.$key.'">';
                      foreach (explode(',', $data) as $key => $value) {
                        $html .= '<input id="'.(100 + $key).'" type="hidden" name="images[]" value="'.$value.'">';
                      }
            $html .= '</div>';

            return $html;
        }else if($type == 'multiple-image' || $type == 'image-slider'){
            $html = '';
            $html .= '<input class="uploadpicker-edit" data-image="#files-'.$key.'" accept="image/*" name="file" type="file" multiple>
                      <div id="files-'.$key.'">';
                      foreach (explode(',', $data) as $key => $value) {
                        $html .= '<input id="'.(100 + $key).'" type="hidden" name="images[]" value="'.$value.'">';
                      }
            $html .= '</div>';

            return $html;
        }else if($type == 'text-block'){
            return '<input class="form-control" value="'.$data.'" type="text" name="text_block">';
        }else if($type == 'text-editor'){
            return '<textarea name="text_editor" class="text-left summernote">'.$data.'</textarea>';
        }else if($type == 'button'){
            $data = json_decode($data);
            return '<div class="form-group text-left"><div class="col-12"><label>Label</label><input class="form-control" type="text" name="button_label" value="'.$data->label.'" class="form-control"></div></div>
                    <div class="form-group text-left"><div class="col-12"><label>Link</label><input class="form-control" type="text" name="button_link" value="'.$data->link.'" class="form-control"></div></div>
                    <div class="form-group text-left"><div class="col-12"><label>Warna</label><select class="form-control" name="button_color"><option '.($data->color == 'red' ? 'selected' : null).' value="red">Merah</option><option '.($data->color == 'orange' ? 'selected' : null).' value="orange">Orange</option><option '.($data->color == 'blue' ? 'selected' : null).' value="blue">Biru</option><option '.($data->color == 'orange_outline' ? 'selected' : null).' value="orange_outline">Orange Outline</option><option '.($data->color == 'blue_outline' ? 'selected' : null).' value="blue_outline">Biru Outline</option></select></div>';
        }else{
            return '';
        }
    }
}
?>