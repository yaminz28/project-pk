<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Design;
use App\Category;
use App\DesignImage;
use App\Creator;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $design = Design::where('creator_id', Auth::user()->id)->get();
        return view('home', ['design' => $design]);
    }
}
