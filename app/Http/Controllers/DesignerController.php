<?php

namespace App\Http\Controllers;

use App\Design;
use App\Category;
use App\DesignImage;
use File;
use Carbon\Carbon;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class DesignerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $design = Design::where('creator_id', Auth::user()->id)->paginate(10);
        return view('design.index', ['design' => $design]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all('id', 'name');
        return view('design.create',compact('categories'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kategoriSort' => 'required',
            'nama' => 'required',
            'pekerjaan' => 'required',
            'umur' => 'required',
            'warna' => 'required',
            'designSort' => 'required',
            'jumlah' => 'required',
            'images' => 'required'
        ]);


        $design = Design::create([
            'creator_id' => e($request->input('creator')),
            'category_id' => e($request->input('kategoriSort')),
            'nama' => e($request->input('nama')),
            'pekerjaan' => e($request->input('pekerjaan')),
            'umur' => e($request->input('umur')),
            'warna' => e($request->input('creator')),
            'gaya_design' => e($request->input('designSort')),
            'jumlah_orang' => e($request->input('jumlah')),
        ]);
        foreach ($request->images as $images) {
            DesignImage::create([
                'image' => $images,
                'design_id' => $design->id
            ]);

        }

        //
        return redirect()->route('designers.index')->with('Success','Design has been created succesfully.');
    }


    public function delete_image(Request $request){
        if ($request->is_new == "false") {
            DesignImage::where('design_id', $request->key)->delete();
        }

        return response()->json(['key' => $request->key, 'is_new' => $request->is_new]);
    }

    public function get_image(Request $request){
        $initialPreview = [];
        $initialPreviewConfig = [];
        $images = DesignImage::where('design_id', $request->id)->get();
        foreach ($images as $image) {
            array_push($initialPreview, $image->image);

            $initialPreviewConfig[] = [
                'caption' => "existing",
                'width' => "120px",
                "url" => url('designers/images/delete'),
                "key" => $image->id,
                "extra" => [
                    "_token" => csrf_token(),
                    "is_new" => false
                ]
            ];
        }
        $data = [
            'initialPreview' => $initialPreview,
            'initialPreviewConfig' => $initialPreviewConfig,
        ];
        return response()->json(['data' => $data]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

            $design = Design::where('id', $id)->first();
            $image = DesignImage::where('design_id', $design->id)->orderByRaw('created_date DESC')->paginate(5);
            return view('design.detail' , ['design' => $design , 'image'=> $image]);

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $design = Design::find($id);
        $categories = Category::all(['id', 'name']);
        return view('design.edit',['design'=>$design, 'categories'=>$categories]);
        //
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $design = Design::find($id);
        $design->category_id = e($request->input('kategoriSort'));
        $design->category_id = e($request->input('kategoriSort'));
        $design->nama = e($request->input('nama'));
        $design->pekerjaan = e($request->input('pekerjaan'));
        $design->umur = e($request->input('umur'));
        $design->warna = e($request->input('warna'));
        $design->gaya_design = e($request->input('designSort'));
        $design->jumlah_orang = e($request->input('jumlah'));
            $design->save();
            if ($request->images) {
                foreach ($request->images as $images) {
                    DesignImage::firstOrCreate([
                        'image' => $images,
                        'design_id' => $design->id
                    ]);
                }
            }
            return redirect()->route('designers.index')->with('success', 'Design has been edited successfully.');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $design = Design::find($id);
        $design->delete();
        return redirect()->route('designers.index')->with('success', 'Design has been deleted successfully.');
        //
    }
}
