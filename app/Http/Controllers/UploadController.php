<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use File;
use Validator;

class UploadController extends Controller
{
    public function upload(Request $request){
        $validation = Validator::make($request->all(), [
            'file' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'type' => 'required'
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors()->first(), 422);
        }else{
            switch ($request->type) {
                case 'design':
                    $image_directory = '/images/designs/';
                    $directory = public_path().$image_directory;
                    if (!File::isDirectory($directory)) {
                        File::makeDirectory($directory);
                    }
                    $url = url('design/images/delete');
                break;
                case 'bohemian':
                    $image_directory = '/images/bohemians/';
                    $directory = public_path().$image_directory;
                    if (!File::isDirectory($directory)) {
                        File::makeDirectory($directory);
                    }
                    $url = url('bohemian/images/delete');
                break;
                case 'skandinafians':
                    $image_directory = '/images/skandinafians/';
                    $directory = public_path().$image_directory;
                    if (!File::isDirectory($directory)) {
                        File::makeDirectory($directory);
                    }
                    $url = url('skandinafians/images/delete');
                break;
                case 'natural':
                    $image_directory = '/images/naturals/';
                    $directory = public_path().$image_directory;
                    if (!File::isDirectory($directory)) {
                        File::makeDirectory($directory);
                    }
                    $url = url('natural/images/delete');
                break;
                case 'modern':
                    $image_directory = '/images/moderns/';
                    $directory = public_path().$image_directory;
                    if (!File::isDirectory($directory)) {
                        File::makeDirectory($directory);
                    }
                    $url = url('modern/images/delete');
                break;
                case 'industrial':
                    $image_directory = '/images/industrials/';
                    $directory = public_path().$image_directory;
                    if (!File::isDirectory($directory)) {
                        File::makeDirectory($directory);
                    }
                    $url = url('industrial/images/delete');
                break;
                case 'futuristik':
                    $image_directory = '/images/futuristik/';
                    $directory = public_path().$image_directory;
                    if (!File::isDirectory($directory)) {
                        File::makeDirectory($directory);
                    }
                    $url = url('futuristik/images/delete');
                break;
            }

            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . request()->file->getClientOriginalExtension();
            request()->file->move($directory, $fileName);

            $data = [
                'initialPreview' => [
                    asset($image_directory.$fileName),
                ],
                'initialPreviewConfig' => [
                    [
                        'caption' => "uploaded",
                        'width' => "120px",
                        "url" => $url,
                        "key" => uniqid(),
                        "extra" => [
                            "_token" => csrf_token(),
                            "is_new" => true
                        ]
                    ]
                ],
                'append' => true // whether to append content to the initial preview (or set false to overwrite)
            ];

            return response()->json($data, 200);
        }
    }
}
