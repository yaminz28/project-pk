<?php

namespace App;


use Illuminate\Foundation\Auth\User as Authenticable;
use Illuminate\Notifications\Notifiable;

class Creator extends Authenticable
{
    protected $fillable = [
        'full_name','display_name','avatar','email','password'
    ];
    public $timestamps = false;
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $cast =[
        'email_verfied_at' =>'datetime',
    ];
    public function creator(){
        return $this->hasOne('App\Creator', 'creator_id');
    }
    //
}
