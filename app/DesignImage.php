<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignImage extends Model
{
    protected $fillable = ['image','design_id'];
    public $timestamps = false;

    public function design_image(){
        return $this->belongsTo('App\Design','design_id');
    }
    //
}
