<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
class Design extends Model
{
    protected $fillable = [
      'id','creator_id','category_id','nama','pekerjaan','umur','warna','gaya_design','jumlah_orang'
    ];
    public $timestamps = false;
    public function creator(){
        return $this->belongsTo('App\Creator','creator_id');
    }
    public function category(){
        return $this->belongsTo('App\Category','category_id');
    }
    public function design_image(){
        return $this->hasMany('App\DesignImage','design_id');
    }
    //
}
