<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth
Route::namespace('Auth')->group(function(){
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login');
    Route::get('logout', 'LoginController@logout')->name('logout');
});
// Registration routes
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

//Root
Route::get('/', function () {
    return redirect('login');
});

//Design
Route::post('/designers/images/delete', 'DesignerController@delete_image')->middleware('auth');
Route::post('/designers/images/get', 'DesignerController@get_image')->middleware('auth');
Route::resource('designers','DesignerController')->middleware('auth');


// Upload
Route::post('upload', 'UploadController@upload')->middleware('auth');
//Profile
Route::get('profile', 'CreatorController@profile');
Route::post('profile', 'CreatorController@update_avatar');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
